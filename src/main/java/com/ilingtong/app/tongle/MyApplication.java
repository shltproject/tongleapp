package com.ilingtong.app.tongle;

import com.ilingtong.library.tongle.TongleAppInstance;

/**
 * Created by wuqian on 2016/5/9.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class MyApplication extends TongleAppInstance {
    @Override
    public void onCreate() {
        super.onCreate();
        setApp_inner_no("01");  //设置版本号
        setPackageName("com.ilingtong.app.tongle");  //设置包名
        setIfRemberKeyName("CKIfRember");    //是否记住密码 sp存储keyname
        setLoginUseridKeyName("CKloginUserid");  //登录用户名 sp存储keyname
        setLoginPwdKeyName("CKloginPwd");   //登录密码 sp存储keyname
        setIfAutoLoginKeyName("CKifAutoLoginKeyName");  //是否自动登录 sp存储keyname
        /********设置微信，微博相关参数*******/
        setSHARE_SDK_ID("128d47dd00c2c");
        setWECHAT_APPID("wxa44de924715e51b9");
        setWECHAT_SECRET("04f00d143acc9c7f52ee9eeab74275b8");
        setSINA_APPKEY("3190168120");
        setSINA_APPSECRET("c1f50c45a85dc39ffdd1e6f189663718");
        setSINA_REDIRECT("http://sns.tongler.cn");
    }
}
